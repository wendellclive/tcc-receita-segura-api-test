package br.com.receitasegura.api.test.services;

import org.junit.BeforeClass;
import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class EspecialidadeTeste {

	@BeforeClass
	public static void setup() {
		RestAssured.baseURI = "http://localhost:8090";
	}	
	
	@Test
	public void deveListarTodasEspecialidades() {
		RestAssured.given()
		.when()
			.get("/especialidades")
		.then()
			.statusCode(200);
	}
	
	@Test
	public void deveInserirUmaEspecialidade() {
		RestAssured.given()
		.body("{\"id\": 9000, \"nomeEspecialidade\": \"TESTE ID 9000\"}")
			.contentType(ContentType.JSON)
		.when()
			.post("/especialidades")
		.then()
			.statusCode(201);
	}
	
	@Test
	public void deveExlcuirUmaEspecialidade() {
		RestAssured.given()
		.when()
			.delete("/especialidades/9000")
		.then()
			.statusCode(204);
	}
	
	public void deveExcluirUmaEspecialidade() {

		RestAssured.given()
		.when()
			.get("/especialidades/9000");

		RestAssured.given()
			.body("{ \"nomeEspecialidade\" : \"\" }")
			.contentType(ContentType.JSON)
		.when()
			.post("/especialidades")
		.then()
			.statusCode(400);
	}


	@Test
	public void deveBuscarUmaEspecialidadeEspecifica() {
		RestAssured.given()
		.when()
			.get("/especialidades/1")
		.then()
			.statusCode(200);
	}
	
	@Test
	public void naoDeveInserirUmaEspecialidadeVazia() {
		RestAssured.given()
			.body("{ \"nomeEspecialidade\" : \"\" }")
			.contentType(ContentType.JSON)
		.when()
			.post("/especialidades")
		.then()
			.statusCode(400);
	}

	@Test
	public void deveAtualizarUmaEspecialidade() {

		RestAssured.given()
			.body("{\"id\": 1, \"dataCadastro\": \"2020-05-04T22:00:54.398355\", \"nomeEspecialidade\": \"TESTE DOBRADO\"}")
			.contentType(ContentType.JSON)
		.when()
			.put("/especialidades/1")
		.then()
			.statusCode(200);
	}
	
}
